package a_mercury_testng.com;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Method_Repository {

	
	WebDriver driver ;
	
	public void browserlaunch() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://newtours.demoaut.com/");
		Thread.sleep(3000);	
	}
	
	public void appclose()
	{
		driver.close();
	}
	
	public void applogin(String username , String password) throws InterruptedException
	{
		WebElement uname =driver.findElement(By.xpath("//input[@name='userName']"));
		uname.sendKeys(username);
		WebElement passwrd =driver.findElement(By.xpath("//input[@name='password']"));
		passwrd.sendKeys(password);
		WebElement login =driver.findElement(By.xpath("//input[@name='login']"));
		login.click();
		Thread.sleep(3000);
	}
	
	public boolean verifylogin()
	{
		String exptitle = "Find a Flight: Mercury Tours: ";
		String acttitle = driver.getTitle();
		if (exptitle.equals(acttitle))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public boolean verifyinvalidlogin()
	{
		String expTitle = "Find a Flight: Mercury Tours:";
		String actTitle = driver.getTitle();
		
		if(expTitle!=actTitle) {
			return true;
		}else {
			return false;
		}
	}
	
public boolean verifyDefaultSelectionRoundTrip() {
		
		WebElement radioBtnOneWay = driver.findElement(By.xpath("//input[@value='oneway']"));
		WebElement radioBtnRoundTrip = driver.findElement(By.xpath("//input[@value='roundtrip']"));
		
		if(radioBtnRoundTrip.isSelected() == true && radioBtnOneWay.isSelected() == false)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
public boolean departingFromValueSelection() throws InterruptedException
{
	WebElement drpdwnDepartingFrom = driver.findElement(By.xpath("//select[@name='fromPort']"));
	Select s1 = new Select(drpdwnDepartingFrom);
	
	Thread.sleep(3000);
	
	s1.selectByVisibleText("London");
	
	Thread.sleep(3000);
	
	if(drpdwnDepartingFrom.getText().equals("London"))
	{
		return true;
	}
	else
	{
		return false;
	}
}
}
