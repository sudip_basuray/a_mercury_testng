package a_mercury_testng.com;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class flightfindersuite
{

	WebDriver driver;
	Method_Repository mr1 =new Method_Repository();
	
	
	@BeforeMethod
	public void applaunch() throws InterruptedException
	{
		mr1.browserlaunch();
	}
	
	/*
	 * Test case 001 for selecting the flight
	 */
	
	@Test(priority = 0, enabled=true, description="TC_001: Verifying default selection of flight type.")
	public void verifyDefaultSelectionFlightType() {
		try {
			mr1.applogin("dasd", "dasd");
			Assert.assertEquals(true, mr1.verifyDefaultSelectionRoundTrip());
		}
		catch(Exception e) {
			System.out.println(e);
			}
		}
	/*
	 * TC_002: Verifying departing from desired value selection.
	 */
	@Test(priority = 1, enabled=true, description="TC_002: Verifying departing from desired value selection.")
	public void verifyDepartingFromValueSelection() {
		try {
			mr1.applogin("dasd", "dasd");
			Assert.assertEquals(true, mr1.departingFromValueSelection());
		}catch(Exception e){
			System.out.println(e);
		}
	}
	@AfterMethod
	public void appClose()
	{
		mr1.appclose();
	}
}
