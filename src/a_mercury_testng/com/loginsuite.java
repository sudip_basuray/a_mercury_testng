package a_mercury_testng.com;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class loginsuite
{
	WebDriver driver;
	Method_Repository mr =new Method_Repository();
	
	@BeforeMethod
	public void applaunch() throws InterruptedException
	{
		mr.browserlaunch();
	}
	/*
	 * test case 001 verify valid login
	 */
	
	@Test(priority = 0 , enabled =true , description = "verify login test 001")
	public void verifylogin()
	{
		try
		{
			mr.applogin("dasd","dasd");
			Assert.assertEquals(true, mr.verifylogin());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			//System.out.println(e);
		}
	}
	@Test(priority =1 ,enabled = true , description="verify invalid login 001")
	public void verifyinvalidlogin()
	{
		try
		{
			mr.applogin("dasda1","dasda1");
			Assert.assertEquals(true, mr.verifyinvalidlogin());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			//System.out.println(e);
		}
	}
	@AfterMethod
	public void appclose()
	{
		mr.appclose();
	}
}
